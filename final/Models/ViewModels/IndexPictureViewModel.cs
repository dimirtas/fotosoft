using System.Collections.Generic;

namespace final.Models.ViewModels
{
    public class IndexPictureViewModel
    {
        public  List<PictureViewModel> PictureViewModels { get; set; }
        public PageViewModel PageViewModel { get; set; }
    }
}
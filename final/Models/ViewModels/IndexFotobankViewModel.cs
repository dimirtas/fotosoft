using System.Collections.Generic;

namespace final.Models.ViewModels
{
    public class IndexFotobankViewModel
    {
        public  List<CatalogViewModel> CatalogViewModels { get; set; }
        public  List<PictureViewModel> PictureViewModels { get; set; }
       
    }
}
using System.Collections.Generic;

namespace final.Models.ViewModels
{
    public class IndexViewModel
    {
        public  List<CatalogViewModel> CatalogViewModels { get; set; }
        public PageViewModel PageViewModel { get; set; }
    }
}
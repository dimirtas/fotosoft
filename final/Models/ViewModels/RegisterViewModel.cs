﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace final.Models.ViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "значение не установлено")]
        [Display(Name = "Email")]
        [RegularExpression(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-‌​]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$", ErrorMessage = "Не верный формат email")]
        [Remote(action: "CheckEmail", controller: "Validation", ErrorMessage = "Данный email уже зарегестрирован")]
        public string Email { get; set; }
        
        [Required(ErrorMessage = "значение не установлено")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "значение не установлено")]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "значение не установлено")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "значение не установлено")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        public string PasswordConfirm { get; set; }

        
        
        
        
        
        
    }
}
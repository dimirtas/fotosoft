﻿using System;

namespace final.Models.ViewModels
{
    public class ProfileViewModel
    {
        public string Id{ get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Avatar { get; set; }
        
        public int PersonalAccount { get; set; }
        
        public int Balance  { get; set; }
       
    }
}
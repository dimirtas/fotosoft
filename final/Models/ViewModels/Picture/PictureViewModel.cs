using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace final.Models.ViewModels
{
    public class PictureViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        
        [Required(ErrorMessage = "вставьте фото")]
         [Display(Name = "Фото")]
        public string Photo { get; set; }
        
        public int? CatalogId { get; set; }
        
      
      
       
    }
}
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace final.Models.ViewModels
{
    public class PictureAddViewModel
    {
        public int Id { get; set; }
        
        [Required(ErrorMessage = "вставьте фото")]
        [Display(Name = "Фото")]
        public IFormFile Photo { get; set; }
        
        [Required(ErrorMessage = "значение не установлено: Название")]
        [Display(Name = "Название общепита")]
        public string Name { get; set; }
       
    }
}
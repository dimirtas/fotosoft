using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace final.Models.ViewModels
{
    public class CatalogViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "значение не установлено: Название")]
        [Display(Name = "Название общепита")]
        public string Name { get; set; }

        public string? Photo { get; set; }
        
        
        public int PhotoCount { get; set; }
       
    }
}
using System.Collections.Generic;

namespace final.Models.ViewModels
{
    public class CatalogDetailsViewModel 
    {
        
        public int Id { get; set; }
        
        public string? Photo { get; set; }

        public string Name { get; set; }

        public List<Picture> Pictures  { get; set; }
       
       
        
    }

}
using System.Collections.Generic;

namespace final.Models
{
    public class Picture
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Photo { get; set; }
        
        public int?  CatalogId { get; set; }
        public Catalog  Catalog { get; set; }

    }
}
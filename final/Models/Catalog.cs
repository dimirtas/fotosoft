using System.Collections.Generic;

namespace final.Models
{
    public class Catalog
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Photo { get; set; }

      
       
        public List<Picture> Pictures { get; set; }

    }
}
﻿using final.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;


namespace final.Data
{
    public class ProjectContext : IdentityDbContext<User>
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Catalog> Catalogs { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        
        
        public ProjectContext(DbContextOptions<ProjectContext> options) : base(options)
        {
        }

      
    }
}
﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using final.Models;
using final.Data;
using final.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace final.Controllers
{
    public class AccountController : Controller
    { 
        
        private ProjectContext _db;
        
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IWebHostEnvironment _webHostEnvironment;
        

        public AccountController(ProjectContext db, UserManager<User> userManager, SignInManager<User> signInManager,IWebHostEnvironment hostEnvironment)
        {
            _db = db;
            _userManager = userManager;
            _signInManager = signInManager;
            _webHostEnvironment = hostEnvironment;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
               
                User user = new User
                {
                    UserName = model.Email, // Юзернейм уникальный емаил
                    Email = model.Email,   
                    Name = model.Name,
                    Surname = model.Surname
                };

                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    
                    await _userManager.AddToRoleAsync(user, "user");
                    await _signInManager.SignInAsync(user, false);
                    return RedirectToAction("Index", "Catalog");
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(String.Empty, error.Description);
                }
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Login(string returnUrl)
        {
            return View(new LoginViewModel {ReturnUrl = returnUrl});
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = new User();
                if (IsEmail(model.Login))
                {
                    user = await _userManager.FindByEmailAsync(model.Login);
                }
                else
                {
                    user = _db.Users.FirstOrDefault(u => u.UserName == model.Login);
                }
                SignInResult result = await _signInManager.PasswordSignInAsync(
                    user,
                    model.Password,
                    model.RememerMe,
                    false
                );
                if (result.Succeeded)
                {
                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                        return Redirect(model.ReturnUrl);
                    {
                        return RedirectToAction("Index", "Catalog");
                    }
            
                    return RedirectToAction("Index", "Catalog");
                }
                ModelState.AddModelError("", "Не верно указан пароль");
            }
            return RedirectToAction("Index", "Catalog");
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Login");
        }
        
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> PersonalProfile()
        {
            ProfileViewModel model = new ProfileViewModel();
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (user != null)
            {
                model.Id = user.Id;
                model.UserName = user.UserName;
                model.Name = user.Name;
                model.Surname = user.Surname;
                

            }

            return View(model);
        }
        
        
        
        public bool IsEmail(string text)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            MatchCollection matchCollection = regex.Matches(text);
            if (matchCollection.Count > 0)
            {
                return true;
            }
            return false;
        }
        
        public string UploadedFile(IFormFile file)
        {
            string uniqueFileName = null;

            if (file != null)
            {
                string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "Files");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + file.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }
                
            }

            return uniqueFileName;
        }

      

    }
}
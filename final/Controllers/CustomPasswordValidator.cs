﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using final.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;

namespace final.Controllers
{
    public class CustomPasswordValidator : IPasswordValidator<User>
    {
        public int RequiredLength { get; set; } // минимальная длина
        
 
        public CustomPasswordValidator(int length)
        {
            RequiredLength = length;
            
        }
 
        public Task<IdentityResult> ValidateAsync(UserManager<User> manager, User user, string password)
        {
            List<IdentityError> errors = new List<IdentityError>();
            
 
            if (String.IsNullOrEmpty(password) || password.Length < RequiredLength)
            {
                errors.Add(new IdentityError
                {
                    Description = $"Минимальная длина пароля равна {RequiredLength}"
                });
            }
            string pattern = "[A-Z]+";
 
            if (!Regex.IsMatch(password, pattern))
            {
                errors.Add(new IdentityError
                {
                    Description = "В пароле должна быть заглавная буква"
                });
            }
            
            string patternNumber = @"[.*\d]";
 
            if (!Regex.IsMatch(password, patternNumber))
            {
                errors.Add(new IdentityError
                {
                    Description = "В пароле должна быть одна цифра"
                });
            }
            string patternSpecial = @"[.*\W]";
 
            if (!Regex.IsMatch(password, patternSpecial))
            {
                errors.Add(new IdentityError
                {
                    Description = "В пароле должен быть один спецсимвол"
                });
            }
            return Task.FromResult(errors.Count == 0 ?
                IdentityResult.Success : IdentityResult.Failed(errors.ToArray()));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using final.Data;
using final.Models;
using final.Models.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace final.Controllers
{
    public class CatalogController : Controller
    {
        private ProjectContext _db;

        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public CatalogController(ProjectContext db, UserManager<User> userManager,
            SignInManager<User> signInManager, IWebHostEnvironment webHostEnvironment)
        {
            _db = db;
            _userManager = userManager;
            _signInManager = signInManager;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int page = 1)
        {
            List<Catalog> catalogs = await _db.Catalogs.ToListAsync();
            List<Picture> pictures = await _db.Pictures.ToListAsync();
            

            List<CatalogViewModel> models = new List<CatalogViewModel>();

            if (catalogs.Count > 0)
            {
                foreach (var catalog in catalogs)
                {
                   
                    
                    models.Add(new CatalogViewModel()
                    {
                        Id = catalog.Id,
                        Name = catalog.Name,
                        Photo = catalog.Photo,
                        PhotoCount = pictures.Where(p => p.CatalogId == catalog.Id).Count(),
                    });
                }
            }

            int pageSize = 9;

            var count = models.Count();
            var items = models.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            IndexViewModel viewModel = new IndexViewModel
            {
                PageViewModel = pageViewModel,
                CatalogViewModels = items
            };

            return View(viewModel);
        }


        [HttpGet]
        public async Task<IActionResult> Add()
        {
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> Add(CatalogAddViewModel model, IFormFile Photo)
        {
            if (ModelState.IsValid)
            {
                Catalog catalog = new Catalog()
                {
                    Name = model.Name,
                    
                };

                if (model.Photo != null && Photo.Length != 0)
                {
                    string exstension = Path.GetExtension(Photo.FileName);
                    string newFileName = Guid.NewGuid() + exstension;

                    string path = "/Files/" + newFileName;
                    // сохраняем файл в папку Files в каталоге wwwroot
                    using (var fileStream = new FileStream(_webHostEnvironment.WebRootPath + path, FileMode.Create))
                    {
                        Photo.CopyTo(fileStream);
                        catalog.Photo = path;
                    }
                }

                if (catalog.Photo == null)
                {
                    string path = "/Files/nofoto2.jpg";
                    catalog.Photo = path;
                }

                _db.Entry(catalog).State = EntityState.Added;
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> Detail(int id)
        {
            Catalog catalog = await _db.Catalogs.FirstOrDefaultAsync(e => e.Id == id);
            List<Picture> pictures =  _db.Pictures.Where(x => x.CatalogId == id).ToList();

            if (catalog is null)
                return NotFound();

            CatalogDetailsViewModel model = new CatalogDetailsViewModel();

            model.Id = catalog.Id;
            model.Photo = catalog.Photo;
            model.Name = catalog.Name;
            model.Pictures = pictures;

            return View(model);
        }
        
        [HttpGet]
        public async Task<IActionResult> DeleteRequest(int? id) // удаление - запрос
        {
            if (id != null)
            {
                Catalog catalog = await _db.Catalogs.FirstOrDefaultAsync(p => p.Id == id );
                if (catalog != null)
                {
                    return View(catalog);
                }
            }

            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> Del(int? id)
        {
            if (id != null)
            {
                Catalog  catalog= await _db.Catalogs.FirstOrDefaultAsync(p => p.Id == id);
                _db.Entry(catalog).State = EntityState.Deleted; 
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return NotFound();
        }
        
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var catalog = _db.Catalogs.FirstOrDefault(p => p.Id == id );

            if (catalog  is null)
                return NotFound();

            CatalogViewModel model = new CatalogViewModel();
            model.Id = catalog.Id;
            model.Name = catalog.Name;
            model.Photo =  catalog.Photo;

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit( CatalogViewModel model , IFormFile Photo)
        {
            if (ModelState.IsValid)
            {
                
                var catalogsChange = _db.Catalogs.FirstOrDefault(p => p.Id == model.Id );
                if ( catalogsChange  is null)
                    return NotFound();
                
                
                catalogsChange.Name = model.Name;
                
               
                if (model.Photo != null && Photo.Length != 0)
                {
                    string exstension = Path.GetExtension(Photo.FileName);
                    string newFileName = Guid.NewGuid() + exstension;

                    string path = "/Files/" + newFileName;
                    // сохраняем файл в папку Files в каталоге wwwroot
                    using (var fileStream = new FileStream(_webHostEnvironment.WebRootPath + path, FileMode.Create))
                    {
                        Photo.CopyTo(fileStream);
                        catalogsChange.Photo = path;
                    }
                }
                _db.Entry(catalogsChange).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }

            
            return RedirectToAction("Index", "Catalog");
        }


    }

}    

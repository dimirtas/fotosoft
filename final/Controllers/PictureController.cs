﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using final.Data;
using final.Models;
using final.Models.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace final.Controllers
{
    public class PictureController : Controller
    {
        private ProjectContext _db;

        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public PictureController(ProjectContext db, UserManager<User> userManager,
            SignInManager<User> signInManager, IWebHostEnvironment webHostEnvironment)
        {
            _db = db;
            _userManager = userManager;
            _signInManager = signInManager;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int page = 1)
        {
            List<Picture> pictures = await _db.Pictures.ToListAsync();

            List<PictureViewModel> models = new List<PictureViewModel>();

            if (pictures.Count > 0)
            {
                foreach (var picture in pictures)
                {
                    models.Add(new PictureViewModel()
                    {
                        Id = picture.Id,
                        Name = picture.Name,
                        Photo = picture.Photo,
                    });
                }
            }

            int pageSize = 9;

            var count = models.Count();
            var items = models.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            IndexPictureViewModel viewModel = new IndexPictureViewModel
            {
                PageViewModel = pageViewModel,
                PictureViewModels = items
            };

            return View(viewModel);
        }


        [HttpGet]
        public async Task<IActionResult> Add()
        {
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> Add (IFormFile Photo)
        {
              if (Photo != null && Photo.Length != 0)
                {
                    Picture picture = new Picture();
                    
                    string exstension = Path.GetExtension(Photo.FileName);
                    string newFileName = Guid.NewGuid() + exstension;

                    string path = "/Files/" + newFileName;
                    // сохраняем файл в папку Files в каталоге wwwroot
                    using (var fileStream = new FileStream(_webHostEnvironment.WebRootPath + path, FileMode.Create))
                    {
                        Photo.CopyTo(fileStream);
                        picture.Photo = path;
                    }
                    
                    _db.Entry(picture).State = EntityState.Added;
                    await _db.SaveChangesAsync();
                    
                    return RedirectToAction("AddToCatalog", "Picture", new {picture.Id});
                }
                return RedirectToAction("Index");
        }
        
        [HttpGet]
        public async Task<IActionResult> AddToCatalog (int Id)
        {
       
            var picture = _db.Pictures.FirstOrDefault(p => p.Id == Id);

            if (picture != null)
            {
                PictureViewModel model = new PictureViewModel();
                model.Id = picture.Id;
                model.Name = picture.Name;
                model.Photo = picture.Photo;
                model.CatalogId = picture.CatalogId;
                
                SelectList catalogList = new SelectList(_db.Catalogs, "Id", "Name");
                ViewBag.CatalogList = catalogList;

                return View(model);
                
            }
            return RedirectToAction("Index");
          
        }
        
        [HttpPost]
        public async Task<IActionResult> AddToCatalog (PictureViewModel model)
        {
            if (ModelState.IsValid)
            {
                var picturesChange = _db.Pictures.FirstOrDefault(p => p.Id == model.Id);
                if (picturesChange is null)
                    return NotFound();

                picturesChange.Name = model.Name;
                picturesChange.CatalogId = model.CatalogId;

                _db.Entry(picturesChange).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }

            return RedirectToAction("Index", "Picture");
          
        }
        

        [HttpGet]
        public async Task<IActionResult> Detail(int id)
        {
            Picture picture = await _db.Pictures.FirstOrDefaultAsync(e => e.Id == id);

            if (picture is null)
                return NotFound();

            PictureDetailsViewModel model = new PictureDetailsViewModel();

            model.Id = picture.Id;
            model.Photo = picture.Photo;
            model.Name = picture.Name;

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> DeleteRequest(int? id) // удаление - запрос
        {
            if (id != null)
            {
                Picture picture = await _db.Pictures.FirstOrDefaultAsync(p => p.Id == id);
                if (picture != null)
                {
                    return View(picture);
                }
            }

            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> Del(int? id)
        {
            if (id != null)
            {
                Picture picture = await _db.Pictures.FirstOrDefaultAsync(p => p.Id == id);
                _db.Entry(picture).State = EntityState.Deleted;
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return NotFound();
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var picture = _db.Pictures.FirstOrDefault(p => p.Id == id);

            if (picture is null)
                return NotFound();

            PictureViewModel model = new PictureViewModel();
            model.Id = picture.Id;
            model.Name = picture.Name;
            model.Photo = picture.Photo;
            
            SelectList catalogList = new SelectList(_db.Catalogs, "Id", "Name");
            ViewBag.CatalogList = catalogList;

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(PictureViewModel model)
        {
            if (ModelState.IsValid)
            {
                var picturesChange = _db.Pictures.FirstOrDefault(p => p.Id == model.Id);
                if (picturesChange is null)
                    return NotFound();

                picturesChange.Name = model.Name;
                if(model.CatalogId !=null)
                    picturesChange.CatalogId = model.CatalogId;

                _db.Entry(picturesChange).State = EntityState.Modified;
                await _db.SaveChangesAsync();
            }

            return RedirectToAction("Index", "Picture");
        }
    }
}
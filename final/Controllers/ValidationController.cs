﻿using System.Collections.Generic;
using System.Linq;
using final.Data;
using final.Models;

using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;


namespace final.Controllers
{
    public class ValidationController
    {
        private ProjectContext _db;
        
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public ValidationController(ProjectContext db, UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _db = db;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [AcceptVerbs("GET", "POST")]
        public bool CheckEmail(string Email)
        {
            List<User> users = _db.Users.ToList();
            var user = users.FirstOrDefault(u => u.Email == Email);
            if (user == null)
            {
                return true;
            }
            return false;
        }
        
        [AcceptVerbs("GET", "POST")]
        public bool CheckUserName(string UserName)
        {
            List<User> users = _db.Users.ToList();
            var user = users.FirstOrDefault(u=> u.UserName == UserName);
            if (user == null)
            {
                return true;
            }
            return false;
        }
        
        [AcceptVerbs("GET", "POST")]
        public bool CheckLogin(string Login)
        {
            List<User> users = _db.Users.ToList();
            var userEmail = users.FirstOrDefault(u=> u.Email == Login);
            var userLogin = users.FirstOrDefault(u=> u.UserName == Login);
            if (userEmail != null  || userLogin != null )
            {
                return true;
            }
            return false;
        }
        
        
       
        [AcceptVerbs("GET", "POST")]
        public bool CheckNumber(string MobilePhone)
        {
            List<User> users = _db.Users.ToList();
            var user = users.FirstOrDefault(u=> u.PhoneNumber == MobilePhone);
            if (user == null)
            {
                return true;
            }
            return false;
        }
        
        [AcceptVerbs("GET", "POST")]
        public bool CheckImage(string Avatar,string ImagePublication)
        {
            string exten = default;
            if (Avatar == null)
            {
                exten = ImagePublication;
            }

            if (ImagePublication == null)
            {
                exten = Avatar;
            }
            string ext = exten.Substring(exten.LastIndexOf('.'));
            if (ext == ".jpg" || ext == ".jpeg" || ext == ".png")
            {
                return true;
            }

            return false;
        }
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using final.Data;
using final.Models;
using final.Models.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace final.Controllers
{
    public class FotoBankController : Controller
    {
        private ProjectContext _db;

        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public  FotoBankController(ProjectContext db, UserManager<User> userManager,
            SignInManager<User> signInManager, IWebHostEnvironment webHostEnvironment)
        {
            _db = db;
            _userManager = userManager;
            _signInManager = signInManager;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int page = 1)
        {
            List<Catalog> catalogs = await _db.Catalogs.ToListAsync();
            List<Picture> pictures = await _db.Pictures.ToListAsync();

            List<CatalogViewModel> catalogsmodel = new List<CatalogViewModel>();

            if (catalogs.Count > 0)
            {
                foreach (var catalog in catalogs)
                {
                    catalogsmodel.Add(new CatalogViewModel()
                    {
                        Id = catalog.Id,
                        Name = catalog.Name,
                        Photo = catalog.Photo,
                        PhotoCount = pictures.Where(p => p.CatalogId == catalog.Id).Count(),
                    });
                }
            }

            List<PictureViewModel> picturesmodel = new List<PictureViewModel>();

            var picturesFree = pictures.Where(p => p.CatalogId == null).ToList();

            if (pictures.Count > 0)
            {
                foreach (var picture in picturesFree)
                {
                    picturesmodel.Add(new PictureViewModel()
                    {
                        Id = picture.Id,
                        Name = picture.Name,
                        Photo = picture.Photo,
                    });
                }
            }

            IndexFotobankViewModel viewModel = new IndexFotobankViewModel();
            viewModel.CatalogViewModels = catalogsmodel;
            viewModel.PictureViewModels = picturesmodel;

            return View(viewModel);
        }

    }

}    
